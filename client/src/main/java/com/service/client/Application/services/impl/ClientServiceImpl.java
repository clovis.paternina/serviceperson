package com.service.client.Application.services.impl;

import com.service.client.Infrastructure.exceptions.GeneralException;
import com.service.client.Infrastructure.exceptions.NotFoundException;
import com.service.client.Domain.model.Client;
import com.service.client.Domain.model.Dto.ClientDTO;
import com.service.client.Domain.model.Dto.FileDTO;
import com.service.client.Infrastructure.rest.mapper.clientMapper.ClientMapper;
import com.service.client.Domain.model.enums.TypeDocument;
import com.service.client.Infrastructure.adapter.ClientRepository;
import com.service.client.Domain.puerto.ClientService;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientMapper clientMapper = Mappers.getMapper(ClientMapper.class);

    private final  ClientRepository clientRepository;

    private final RabbitMQImpl rabbitMQImpl;

    public ClientServiceImpl(ClientRepository clientRepository, RabbitMQImpl rabbitMQSender) {
        this.clientRepository = clientRepository;
        this.rabbitMQImpl = rabbitMQSender;
    }

    @Override
    public ClientDTO save(ClientDTO clientDTO, MultipartFile file) throws IOException {
        Client clientSaved = clientRepository.save(clientMapper.clientDTOToClient(clientDTO));

        FileDTO photo = new FileDTO();
        photo.setId(clientDTO.getIdentification().toString());
        photo.setTitle(file.getOriginalFilename());
        photo.setImage(Base64Utils.encode(file.getBytes()));

        rabbitMQImpl.send(photo);
        return clientMapper.clientToClientDTO(clientSaved);
    }

    @Override
    public List<ClientDTO> listAll() {
        List<Client> clientList = clientRepository.findAll();
        return clientMapper.map(clientList);
    }

    @Override
    public ClientDTO update(ClientDTO clientDto,Long id) {
        if(clientRepository.existsById(id)) {
            Client client = clientMapper.clientDTOToClient(clientDto);
            client.setId(id);
            Client clientSaved = clientRepository.save(client);
            return clientMapper.clientToClientDTO(clientSaved);
        }

        throw new NotFoundException("Id does not exits!!!");

    }

    @Override
    public Boolean delete(Long id) {
       if(clientRepository.existsById(id)){
           clientRepository.deleteById(id);
           return true;
       }else{
           return false;
       }
    }

    @Override
    public ClientDTO findById(Long id) {
        if(clientRepository.existsById(id)){
           Client client = clientRepository.findById(id).get();
            return clientMapper.clientToClientDTO(client);
        }
        throw new GeneralException("Id does not exit!!!");
    }

    @Override
    public List<ClientDTO> findClientByAge(Long age) {

        return clientMapper.map(clientRepository.findClientByAge(age));
    }

    @Override
    public ClientDTO findByTypeDocAndIdentification( TypeDocument typeDocument, Long identification) {
           Client client = clientRepository.findByTypeDocAndIdentification( typeDocument, identification);
        return clientMapper.clientToClientDTO(client);
    }

    @Override
    public List<ClientDTO> findByTypeDoc(TypeDocument typeDocument) {
      List<Client> listClient = clientRepository.findByTypeDoc(typeDocument);
        if(!listClient.isEmpty()){
            return clientMapper.map(listClient);
        }else{
            throw new GeneralException("No existen Clientes con este Tipo de doc "+ typeDocument);
        }
    }

}
