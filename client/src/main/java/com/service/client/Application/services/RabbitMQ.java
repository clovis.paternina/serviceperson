package com.service.client.Application.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.service.client.Domain.model.Dto.FileDTO;

public interface RabbitMQ {
    void send (FileDTO fileDto) throws JsonProcessingException;
}
