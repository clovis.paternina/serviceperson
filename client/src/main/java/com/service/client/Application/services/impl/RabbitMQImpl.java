package com.service.client.Application.services.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.client.Application.services.RabbitMQ;
import com.service.client.Domain.model.Dto.FileDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQImpl implements RabbitMQ {

    @Autowired
    private AmqpTemplate rabbitTemplate;
    @Autowired
    private Queue queue;

    private static Logger logger = LogManager.getLogger(RabbitMQ.class.toString());

    @Override
    public void send(FileDTO fileDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String fileDtoString = objectMapper.writeValueAsString(fileDto);
        rabbitTemplate.convertAndSend(queue.getName(), fileDtoString);
        logger.info("Sending Message to the Queue : " + fileDto.toString());
    }
}
