package com.service.client.Infrastructure.rest.mapper.clientMapper;


import com.service.client.Domain.model.Client;
import com.service.client.Domain.model.Dto.ClientDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ClientMapper {
    ClientDTO clientToClientDTO (Client client);
    Client clientDTOToClient (ClientDTO clientDTO);
    List<ClientDTO> map(List<Client> clientList);


}
