package com.service.client.Infrastructure.rest.controllers;

import com.service.client.Domain.model.Dto.ClientDTO;
import com.service.client.Domain.model.enums.TypeDocument;
import com.service.client.Application.services.impl.ClientServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/client")
public class ClientController {


   private final ClientServiceImpl clientServiceImpl;

   public ClientController(ClientServiceImpl clientServiceImpl) {
       this.clientServiceImpl = clientServiceImpl;
   }

    @Operation(summary = "Save person")
    @PostMapping(name = "/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE} )
    public ResponseEntity<ClientDTO> save(@RequestPart @Valid ClientDTO clientDto, @RequestPart("image") MultipartFile image )throws IOException {
        ClientDTO clientDTOSaved = clientServiceImpl.save(clientDto, image);
        return new ResponseEntity<>(clientDTOSaved, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDTO> getClientById(@PathVariable( value = "id") Long idClient) {
        return new ResponseEntity<>( clientServiceImpl.findById(idClient),HttpStatus.ACCEPTED);
    }
    @GetMapping("age/{age}")
    public ResponseEntity<List <ClientDTO>> getClientByAge(@RequestParam( value = "age") Long age) {
        return new ResponseEntity<>( clientServiceImpl.findClientByAge(age),HttpStatus.ACCEPTED);
    }

    @GetMapping("/typeDoc/{typeDoc}/identification/{identification}")
    public ResponseEntity< ClientDTO> getClientByTypeDoc(@PathVariable( value = "identification") Long identification, @PathVariable( value = "typeDoc") TypeDocument typeDocument) {
        return new ResponseEntity<>( clientServiceImpl.findByTypeDocAndIdentification( typeDocument, identification),HttpStatus.ACCEPTED);
    }
    @GetMapping("/typeDoc/{typeDoc}")
    public ResponseEntity<List<ClientDTO> > getClientByTy( @PathVariable( value = "typeDoc") TypeDocument typeDocument) {
        return new ResponseEntity<>( clientServiceImpl.findByTypeDoc(typeDocument),HttpStatus.ACCEPTED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ClientDTO> update(@RequestBody ClientDTO clientDTO,@PathVariable(value ="id") Long id) {
        return new ResponseEntity<>( clientServiceImpl.update(clientDTO,id),HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") Long idPerson) {
       if (clientServiceImpl.delete(idPerson)){
           return new ResponseEntity<>("Client deleted successfully",HttpStatus.ACCEPTED);
       }

       return new ResponseEntity<>("Client does not exit!!!",HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping("/all")
    public List<ClientDTO> getAll() {
       List<ClientDTO> clients = clientServiceImpl.listAll();
       return clients;
    }

}
