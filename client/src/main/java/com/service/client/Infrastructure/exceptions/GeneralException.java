package com.service.client.Infrastructure.exceptions;

public class GeneralException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String msg;

    public GeneralException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}