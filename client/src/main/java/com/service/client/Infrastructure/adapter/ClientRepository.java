package com.service.client.Infrastructure.adapter;

import com.service.client.Domain.model.Client;
import com.service.client.Domain.model.enums.TypeDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("Select client from Client client where client.age >= :age")
    List<Client> findClientByAge (@Param("age") Long age);

    Client findByTypeDocAndIdentification(TypeDocument typeDocument, Long identification);
    List<Client> findByTypeDoc( TypeDocument typeDocument);
}
