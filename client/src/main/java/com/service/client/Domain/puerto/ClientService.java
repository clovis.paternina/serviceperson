package com.service.client.Domain.puerto;

import com.service.client.Domain.model.Dto.ClientDTO;
import com.service.client.Domain.model.enums.TypeDocument;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ClientService {

    ClientDTO save(ClientDTO clientDTO,MultipartFile image) throws IOException;
    List<ClientDTO> listAll();
    ClientDTO update(ClientDTO clientDTO,Long id);
    Boolean delete(Long id);
    ClientDTO findById(Long id);

    List<ClientDTO> findClientByAge(Long age);

    ClientDTO findByTypeDocAndIdentification( TypeDocument typeDocument, Long identication);
    List<ClientDTO> findByTypeDoc( TypeDocument typeDocument);

}
