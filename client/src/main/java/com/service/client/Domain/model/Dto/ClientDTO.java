package com.service.client.Domain.model.Dto;

import com.service.client.Domain.model.enums.TypeDocument;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientDTO implements Serializable  {

    String name;
    String lastName;
    TypeDocument typeDoc;
    Long identification;
    Long age;
    String city;
}
