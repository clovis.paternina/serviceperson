package com.service.client.Domain.model;

import com.service.client.Domain.model.enums.TypeDocument;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Client {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    Long id;

    @NonNull
    String name;

    @NonNull
    String lastName;

    @NonNull
    @Enumerated(EnumType.STRING)
    TypeDocument typeDoc;

    @NonNull
    @Column(unique = true)
    Long identification;

    Long age;
    String city;
}
