package com.service.client.services.impl;

import com.service.client.Application.services.impl.ClientServiceImpl;
import com.service.client.Application.services.impl.RabbitMQImpl;
import com.service.client.Domain.model.enums.TypeDocument;
import com.service.client.Infrastructure.adapter.ClientRepository;
import com.service.client.Domain.model.Client;
import com.service.client.Domain.model.Dto.ClientDTO;
import com.service.client.Domain.puerto.ClientService;
import com.service.client.Infrastructure.exceptions.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

class ClientServiceImplTest {

    private ClientService clientService;
    @Mock
    private ClientRepository clientRepository;

    @Mock
    private  RabbitMQImpl rabbitMQImpl;

    private ClientDTO clientDTO;
    private Client client;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);
        clientService = new ClientServiceImpl(clientRepository, rabbitMQImpl);
        clientDTO = ClientDTO.builder()
                .name("carlos")
                .typeDoc(TypeDocument.CC)
                .lastName("paternina")
                .identification(234234234L)
                .city("rionegro")
                .build();

        client = Client.builder()
                .name("carlos")
                .typeDoc(TypeDocument.CC)
                .lastName("paternina")
                .identification(234234234L)
                .city("rionegro")
                .build();
    }

    @Test
    public void saveTest() throws IOException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", new byte[]{1});
       clientService.save(clientDTO, multipartFile );
        Mockito.verify(clientRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(rabbitMQImpl, Mockito.times(1)).send(Mockito.any());
    }

    @Test
    public void listAllTest() {
        Mockito.when(clientRepository.findAll()).thenReturn(Arrays.asList(client));
        List <ClientDTO> listClient = clientService.listAll();
        Assertions.assertTrue(!listClient.isEmpty());
        org.assertj.core.api.Assertions.assertThat(listClient.size()).isEqualTo(1);
    }
    @Test
    public void updateTest() {
        Long id = 1L;
        Mockito.when(clientRepository.existsById(id)).thenReturn(true);
        clientService.update(clientDTO, id);
        Mockito.verify(clientRepository, Mockito.times(1)).save(Mockito.any());
    }
    @Test()
    public void updateInvalidTest() {
        Long id = 1L;
        Mockito.when(clientRepository.existsById(id)).thenReturn(false);
        NotFoundException notFoundException = Assertions.assertThrows(NotFoundException.class, () -> {

            clientService.update(clientDTO, id);
        }, "Not found");

        Assertions.assertEquals("Id does not exits!!!", notFoundException.getMsg() );

    }
    @Test
    public void deleteSuccessTest() {
        Long id = 1L;
        Mockito.when(clientRepository.existsById(id)).thenReturn(true);
        Boolean success = clientService.delete(id);
        Mockito.verify(clientRepository, Mockito.times(1)).deleteById(id);
        Assertions.assertTrue(success);
    }

    @Test
    public void deleteFailedTest() {
        Long id = 1L;
        Mockito.when(clientRepository.existsById(id)).thenReturn(false);
        Boolean success = clientService.delete(id);
        Mockito.verify(clientRepository, Mockito.never()).deleteById(id);
        Assertions.assertFalse(success);
    }

    @Test
    public  void findClientByAgeTest() {
        Long age = 10L;
        Mockito.when(clientRepository.findClientByAge(age)).thenReturn(Arrays.asList(client));
        List <ClientDTO> listClient = clientService.findClientByAge(age);
        Assertions.assertTrue(!listClient.isEmpty());
        org.assertj.core.api.Assertions.assertThat(listClient.size()).isEqualTo(1);
    }



}